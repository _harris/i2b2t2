-- SQL SERVER definition for the Tableau Data Extract Request table.
CREATE TABLE [dbo].[TABLEAU_EXTRACT_REQUEST](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EXTRACT_CREATE_DATE] [datetime] NOT NULL DEFAULT (getdate()),
	[QUERY_MASTER_ID] [int] NOT NULL,
	[USER_ID] [varchar](50) NOT NULL,
	[QUERY_NAME] [varchar](100) NULL,
	[GENERATED_SQL] [varchar](max) NOT NULL,
	[STATUS_CD] [char](1) NOT NULL DEFAULT ('A'),
	[EXPIRE_DATE] [date] NOT NULL DEFAULT (dateadd(day,(30),getdate())),
	[PATIENT_HASH_SALT] [nvarchar](66) NULL,
	PRIMARY KEY CLUSTERED ([ID] ASC)
);


