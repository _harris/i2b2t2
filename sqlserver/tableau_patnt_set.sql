-- SQLSERVER definition for Tableau patient set table
CREATE TABLE [dbo].[TABLEAU_PATNT_SET](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TABLEAU_EXTRACT_ID] [int] NOT NULL,
	[PATIENT_NUM] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([ID] ASC)
); 