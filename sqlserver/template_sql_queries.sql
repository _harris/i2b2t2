-- SQLSERVER create script for table that holds SQL templates
CREATE TABLE [dbo].[SQL_QUERIES](
	[DIMENSION_NAME] [varchar](50) NOT NULL,
	[SQL_TXT] [nvarchar](max) NULL,
	PRIMARY KEY CLUSTERED ([DIMENSION_NAME] ASC)
);
