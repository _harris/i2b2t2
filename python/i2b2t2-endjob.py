# For a given query, mark the query comlete (or flag an error).
# 
# - Daniel R. Harris
import pyodbc
from sys import argv

cxn = pyodbc.connect('DSN=<insert your DSN here>',autocommit=True)
cursor = cxn.cursor()

script, prefix = argv
extract_id = prefix.split('_')[-1]

running_query = 'select status_cd from dbo.TABLEAU_EXTRACT_REQUEST where id=?'
status_query = '''update dbo.TABLEAU_EXTRACT_REQUEST
		set STATUS_CD = ?
		where id = ?'''

try:
	cursor.execute(running_query, extract_id)
	row = cursor.fetchone()
	if row.status_cd == 'R':
		cursor.execute(status_query, 'D', extract_id)
except Exception as e:
	print e
	cursor.execute(status_query, 'E', extract_id)

cxn.close()
