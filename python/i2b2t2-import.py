# For a given query and a given dimension, drop data 
# and TDE files into the directory specified.
#
# You must have the schema files so that the columns
# and data types are known.
#
# -Daniel R. Harris
#
import dataextract as tde
import os
import datetime
import pyodbc
import csv
from sys import argv, exit

# Map strings in config file to Tableau data types
def str_to_type( string ):
	if string == 'CHAR_STRING':
		return tde.Type.CHAR_STRING
	elif string == 'INTEGER':
		return tde.Type.INTEGER
	elif string == 'DATETIME':
		return tde.Type.DATETIME
	elif string == 'DOUBLE':
		return tde.Type.DOUBLE
	else:
		return tde.Type.CHAR_STRING

cxn = pyodbc.connect('DSN=<INSERT_YOUR_DSN_HERE>')
cursor = cxn.cursor()

script, prefix, tableau_directory, dimension, table_string = argv
tableau_id = prefix.split('_')[-1]
job_query_string = "select sql_txt from " + table_string + " where TABLEAU_REQUEST_ID = " + tableau_id

try:
	cursor.execute(job_query_string)
	row = cursor.fetchone()
	if row is not None:
		query_string = row.sql_txt
except Exception as e:
	print e
	exit()

tde_filename    = tableau_directory + '/Data/extract_' + dimension + '.tde'
dsv_filename    = tableau_directory + '/csv/extract_' + dimension + '.csv'
schema_filename	= 'schemas/' + dimension + '.csv'


if os.path.isfile(tde_filename):
	os.remove(tde_filename)
tdefile = tde.Extract(tde_filename)

tabledef = tde.TableDefinition()

dsvfile = open(dsv_filename, "w")

# With great schema, comes great responsibility:
with open(schema_filename, "rb") as tde_schema:
	schema_reader = csv.DictReader(tde_schema, skipinitialspace=True)
	for record in schema_reader:
		tabledef.addColumn(record['tb_name'], str_to_type(record['tb_data_type']))
	tde_table = tdefile.addTable('Extract', tabledef)

	cursor.execute(query_string)

	newrow = tde.Row(tabledef)
	# note to self, test fetchall on large extract
	for row in cursor.fetchall():
		tde_schema.seek(0)
		schema_reader.next()
		for record in schema_reader:
			column_num = int(record['column_num'] or 0)
			column_value = row[int(record['column_num'] or 0)]
			if record['tb_data_type'] == 'CHAR_STRING':
				if 's' in record['col_flags']:
					newrow.setCharString(column_num,\
					'%.f' % column_value)
				else:
					newrow.setCharString(column_num,\
						column_value or "None")
			if record['tb_data_type'] == 'INTEGER':
				newrow.setInteger(column_num,\
					int(column_value or 0))
			if record['tb_data_type'] == 'DOUBLE':
				newrow.setDouble(column_num,\
					float(column_value or 0))
			if column_value is not None and record['tb_data_type'] == 'DATETIME':
				newrow.setDateTime(column_num,\
					column_value.year, column_value.month,\
					column_value.day, column_value.hour,\
					column_value.minute, column_value.second, 0)
		tde_table.insert(newrow)
		csv.writer(dsvfile, delimiter="|").writerow(row)
tdefile.close()
dsvfile.close()
cxn.close()
