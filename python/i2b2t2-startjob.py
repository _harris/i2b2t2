# This selects the first pending data extract
# and executes the initialization procedure for the job.
# After this runs, there should be an encounter table
# and queries logged for this particular extract.
# If no requests are pending, nothing happens.
#
# - Daniel R. Harris
#
import pyodbc

cxn = pyodbc.connect('DSN=<INSERT YOUR DSN HERE>',autocommit=True)
cursor = cxn.cursor()
cxn.autocommit = True

views_query = '''select top 1 id, query_master_id, user_id, 
			query_name
		from dbo.TABLEAU_EXTRACT_REQUEST
		where STATUS_CD = 'A';'''
status_query = '''update dbo.TABLEAU_EXTRACT_REQUEST
		set STATUS_CD = ?
		where id = ?'''


try:
	cursor.execute(views_query)
	row = cursor.fetchone()
	if row is not None:
		tid = str(row.id)
		print row.user_id + '_' + str(row.query_master_id) + '_' + tid
		cursor.execute(status_query, 'R', tid)
		cursor.execute("exec init_tableau_job ?;", tid)
except Exception as e:
	print e
	cursor.execute(status_query, 'E', row.id)
try:
	cxn.commit()
except Exception as e:
	pass

cxn.close()
