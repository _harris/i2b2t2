#!/bin/bash
# Main control script. Schedule as a cron job at desired interval
# or run as needed.
#
# Set base_dir as a location for all jobs and resulting files to go.
#

base_dir=/media/tableau/
sed_pattern="<INSERT_ORIGINAL_DATASOURCE_LOCATION>"
workbooks_dir="${base_dir}_workbooks/"
workbook_template="template.twb"

prefix=`python i2b2t2-startjob.py`

if [ -z "$prefix" ]; then
	exit 0
fi

tableau_dir="${base_dir}${prefix}_extract"
tableau_workbook="${prefix}_extract.twb"

mkdir -p $tableau_dir/Data
mkdir -p $tableau_dir/csv

cp ${workbooks_dir}${workbook_template} $tableau_dir/$tableau_workbook

# Optional line, use if you need to remove origina template location
#sed -i 's/${sed_pattern}//g' $tableau_dir/$tableau_workbook

python i2b2t2-import.py $prefix $tableau_dir 'encounter' 'TDE_DEV.PATNT_ENCNTR'
python i2b2t2-import.py $prefix $tableau_dir 'diagnosis' 'TDE_DEV.PATNT_ENCNTR_DX'
python i2b2t2-import.py $prefix $tableau_dir 'labs' 'TDE_DEV.PATNT_ENCNTR_LABS'
python i2b2t2-import.py $prefix $tableau_dir 'meds' 'TDE_DEV.PATNT_ENCNTR_MEDS'
python i2b2t2-import.py $prefix $tableau_dir 'procedure' 'TDE_DEV.PATNT_ENCNTR_PROC'
python i2b2t2-endjob.py $prefix

cd $tableau_dir
zip -r ${tableau_workbook}x Data $tableau_workbook
zip -r ${prefix}_extract.zip csv ${tableau_workbook}x
